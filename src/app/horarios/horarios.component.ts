import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, Validators} from '@angular/forms';
import {AlertType} from '../core/utility/alert/typeAlert';
import {Router} from '@angular/router';
import {faSmile} from '@fortawesome/free-solid-svg-icons';
import {AlertService} from "../core/utility/alert/alert.service";

@Component({
  selector: 'app-horarios',
  templateUrl: './horarios.component.html',
  styleUrls: ['./horarios.component.scss']
})
export class HorariosComponent implements OnInit {

  faSmile = faSmile;
  form = this.fb.group({
    data: new FormControl(Validators.required),
    horario: new FormControl(Validators.required)
  });

  horarios = [
    {id: 1, descricao: '00:00'},
    {id: 2, descricao: '01:00'},
    {id: 3, descricao: '02:00'},
    {id: 4, descricao: '03:00'},
    {id: 5, descricao: '04:00'},
    {id: 6, descricao: '05:00'},
    {id: 7, descricao: '06:00'},
    {id: 8, descricao: '07:00'},
    {id: 9, descricao: '08:00'},
    {id: 10, descricao: '09:00'},
    {id: 11, descricao: '10:00'},
    {id: 12, descricao: '11:00'},
    {id: 13, descricao: '12:00'},
    {id: 14, descricao: '13:00'},
    {id: 15, descricao: '14:00'},
    {id: 16, descricao: '15:00'},
    {id: 17, descricao: '16:00'},
    {id: 18, descricao: '17:00'},
    {id: 19, descricao: '18:00'},
    {id: 20, descricao: '19:00'},
    {id: 21, descricao: '20:00'},
    {id: 22, descricao: '21:00'},
    {id: 23, descricao: '22:00'},
    {id: 24, descricao: '23:00'}
  ];

  constructor(private fb: FormBuilder, private router: Router, private alertService: AlertService) {
  }

  ngOnInit(): void {
  }

  save() {
    this.router.navigate(['/consulta']);
    setTimeout(() => {
      this.alertService.send({
        message: 'Cadastrado com sucesso!',
        type: AlertType.Success,
        icon: faSmile
      });
    }, 200);
  }

}
