import {Component, Input, OnInit} from '@angular/core';
import {faBook} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'custom-table',
  templateUrl: './custom-table.component.html',
  styleUrls: ['./custom-table.component.scss']
})
export class CustomTableComponent implements OnInit {

  @Input() headers;
  @Input() consulta;
  @Input() solicitacao;
  @Input() clientes;

  faBook = faBook;

  constructor() {
  }

  ngOnInit(): void {
  }

}
