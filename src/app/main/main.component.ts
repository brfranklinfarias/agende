import {Component, Input, OnInit} from '@angular/core';
import {faArrowCircleLeft} from '@fortawesome/free-solid-svg-icons/faArrowCircleLeft';
import {faPlus} from '@fortawesome/free-solid-svg-icons/faPlus';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';

@Component({
  selector: 'main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  @Input() title: string;
  private history = [];
  faArrowClicleLeft = faArrowCircleLeft;
  faArrowLeft = faArrowCircleLeft;
  visiblePlus = true;
  faPlus = faPlus;
  current;

  constructor(private route: ActivatedRoute, private router: Router, private location: Location) {
  }

  ngOnInit(): void {
    switch (this.route.snapshot.routeConfig.path) {
      case 'agenda':
        this.visiblePlus = false;
        break;
      case 'solicitacao':
        this.visiblePlus = false;
        break;
      case 'agendamento/:id':
        this.visiblePlus = false;
        break;
      case 'horarios':
        this.visiblePlus = false;
        break;
      case 'tipoConsulta':
        this.visiblePlus = false;
        break;
      case 'clientes':
        this.visiblePlus = false;
        break;
    }
  }

  goBack() {
    if (this.location.path() !== '/home') {
      this.location.back();
    }
  }
}
