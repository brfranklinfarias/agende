import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-solicitacao',
  templateUrl: './solicitacao.component.html',
  styleUrls: ['./solicitacao.component.scss']
})
export class SolicitacaoComponent implements OnInit {

  tableHeader =
    [
      {id: 1, description: 'Profissional'}, {id: 2, description: 'Especialidade'},
      {id: 3, description: 'CRO'}, {id: 4, description: 'Agenda'}
    ];

  solicitacao = [
    {
      id: 1,
      profissional: [
        {
          nome: 'Dr. José dos Santos',
          especialidade: 'Ortodontista',
          cro: '125489'
        }
      ]
    },
    {
      id: 2,
      profissional: [
        {
          nome: 'Dr. Rafael Rabelo',
          especialidade: 'Ortodontista',
          cro: '25489'
        }
      ]
    }
  ];


  constructor() {
  }

  ngOnInit(): void {
  }

}
