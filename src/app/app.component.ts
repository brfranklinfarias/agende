import {Component, OnInit} from '@angular/core';
import {AlertService} from './core/utility/alert/alert.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  alert;

  constructor(private alertService: AlertService) {
  }

  ngOnInit(): void {
    this.alertService.receive().subscribe(alert => {
      this.alert = alert;
    });
  }

}
