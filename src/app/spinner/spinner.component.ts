import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {TweenMax} from 'gsap';

@Component({
  selector: 'app-spinner',
  templateUrl: './spinner.component.html',
  styleUrls: ['./spinner.component.scss']
})
export class SpinnerComponent implements OnInit {

  @ViewChild('box') box: ElementRef;

  constructor() {
  }

  ngOnInit(): void {
    setTimeout(() => {
      this.loading();
    }, 200);
  }

  loading() {
    TweenMax.to(this.box.nativeElement, 1, {rotateY: '+=180', repeat:-1, yoyo: true});
  }
}
