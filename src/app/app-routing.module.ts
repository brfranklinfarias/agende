import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './login/login.component';
import {ConsultaComponent} from './consulta/consulta.component';
import {SolicitacaoComponent} from './solicitacao/solicitacao.component';
import {RegisterComponent} from './login/register/register.component';
import {AgendaListComponent} from './agenda/agenda-list.component';
import {MenuComponent} from './menu/menu.component';
import {HorariosComponent} from './horarios/horarios.component';
import {TipoConsultaComponent} from './tipo-consulta/tipo-consulta.component';
import {ClienteComponent} from "./cliente/cliente.component";


const routes: Routes = [
  {path: '', redirectTo: 'login', pathMatch: 'full'},
  {path: 'login', component: LoginComponent},
  {path: 'cadastro', component: RegisterComponent},
  {path: 'consulta', component: ConsultaComponent},
  {path: 'menu', component: MenuComponent},
  {path: 'solicitacao', component: SolicitacaoComponent},
  {path: 'horarios', component: HorariosComponent},
  {path: 'tipoConsulta', component: TipoConsultaComponent},
  {path: 'clientes', component: ClienteComponent},
  {path: 'agendamento/:id', component: AgendaListComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
