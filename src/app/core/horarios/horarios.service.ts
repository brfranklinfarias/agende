import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Horario} from './horario';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class HorariosService {
  private api = environment.api;

  constructor(private httpClient: HttpClient) {
  }

  list(): Observable<Horario[]> {
    return this.httpClient.get<Horario[]>(`${this.api}/horario`);
  }

}
