import {Cliente} from "../cliente/cliente";

export class Atendimento {
  id: number;
  horario: string;
  turno: string;
  status: string;
  cliente: Cliente;

  constructor(obj?: Object) {
    let object: any = Object.assign(obj);
    for (let prop in object) {
      this[prop] = object[prop];
    }
  }

}
