import {AlertType} from "../alert/typeAlert";
import {Icon, IconDefinition} from "@fortawesome/fontawesome-svg-core";

export class Alert {
  message: string;
  type: AlertType;
  icon?: IconDefinition;

  constructor(obj?: any) {
    let object: any = Object.assign(obj);
    for(let prop in object) {
      if(object.hasOwnProperty('icon')){
        this.icon = object['icon'];
        delete object['icon'];
      }
      this[prop] = object[prop];
    }
  }
}
