import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {Alert} from './alert';

@Injectable({
  providedIn: 'root'
})
export class AlertService {
  private subject: Subject<Alert> = new Subject<Alert>();
  private alert: Alert = new Alert({});

  constructor() {
  }

  receive(): Observable<Alert> {
    return this.subject.asObservable();
  }

  send(alert: Alert) {
    this.alert = alert;
    this.subject.next(this.alert);
  }
}
