import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Estado} from './estado';

@Injectable({
  providedIn: 'root'
})
export class EstadoService {

  constructor(private httpClient: HttpClient) { }

  getEstados(): Observable<Estado> {
    return this.httpClient.get<Estado>('https://servicodados.ibge.gov.br/api/v1/localidades/estados');
  }

  getCidadesByUF(uf: string): Observable<Estado> {
    return this.httpClient.get<Estado>(`https://servicodados.ibge.gov.br/api/v1/localidades/estados/${uf}/distritos`);
  }
}
