import {ErrorHandler, Injectable} from '@angular/core';
import {User} from "../usuario/user";
import {Router} from "@angular/router";
import {UserService} from "../usuario/user.service";
import {AlertService} from "../utility/alert/alert.service";
import {AlertType} from "../utility/alert/typeAlert";
import {faSadTear} from "@fortawesome/free-solid-svg-icons";
import {BehaviorSubject, Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {map} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  private userAuthentication: boolean = false;
  users: User[] = [];
  err: Response;
  faSad = faSadTear;
  private currentUserSubject: BehaviorSubject<any>;
  public currentUser: Observable<any>;

  constructor(private router: Router,
              private error: ErrorHandler,
              private httpClient: HttpClient,
              private userService: UserService) {
    this.currentUserSubject = new BehaviorSubject<any>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

 logout() {
    localStorage.removeItem('currentUser');
    this.currentUserSubject.next(null);
 }
}
