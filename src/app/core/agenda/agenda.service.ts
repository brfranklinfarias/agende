import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';
import {Agendamento} from '../agendamento/agendamento';
import {Profissional} from '../profissional/profissional';

@Injectable({
  providedIn: 'root'
})
export class AgendaService {

  private api = environment.api;
  httpOptions = {
   headers: new HttpHeaders({
     'Content-Type': 'application/json'
   })
  };

  constructor(private httpClient: HttpClient) {
  }


  get(id: number): Observable<Agendamento> {
    return this.httpClient.get<Agendamento>(`${this.api}/agendamentos/${id}`);
  }

  list(): Observable<Profissional[]> {
    return this.httpClient.get<Profissional[]>(`${this.api}/profissional`);
  }
}
