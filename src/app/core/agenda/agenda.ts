import {Atendimento} from '../atendimento/atendimento';

export class Agenda {
  id: number;
  dia: string;
  turno: string;
  horarioInicio: string;
  horarioFim: string;

  constructor(obj?: Object) {
    let object: any = Object.assign(obj);
    for (let prop in object) {
      this[prop] = object[prop];
    }
  }

 /* getAgendas(agendas: Agenda[]): Agenda[] {
    return agendas;
  }

  getAtendimentos(agenda: Agenda[]): Atendimento[] {
    const atendimentos: Atendimento[] = [];
    agenda.forEach(agenda => {
      agenda.atendimentos.map(atendimento => {
        atendimentos.push(atendimento);
      });
    });
    return atendimentos;
  }

  getAtendimentoByData(agenda?: Agenda[], data?: string): Atendimento[] {
    let atendimentos: Atendimento[] = [];
    agenda.forEach(agenda => {
      agenda.atendimentos.filter((atendimento) => {
        if (agenda.data == data) {
          atendimentos.push(atendimento);
        }
      });
    });
    return atendimentos;
  }

  getAtendimentosTurno(agenda: Agenda[], turno: string): Atendimento[] {
    let atendimentos: Atendimento[] = [];
    agenda.forEach(agenda => {
      agenda.atendimentos.filter((atendimento) => {
        if (atendimento.turno == turno) {
          atendimentos.push(atendimento);
        }
      });
    });
    return atendimentos;
  }*/
}
