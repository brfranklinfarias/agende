import { Injectable } from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Agendamento} from '../agendamento/agendamento';
import {Profissional} from './profissional';

@Injectable({
  providedIn: 'root'
})
export class ProfissionalService {

  private api = environment.api;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  constructor(private httpClient: HttpClient) {
  }


  get(id: number): Observable<Profissional> {
    return this.httpClient.get<Profissional>(`${this.api}/profissional/${id}`);
  }

  list(): Observable<Profissional[]> {
    return this.httpClient.get<Profissional[]>(`${this.api}/profissional`);
  }}
