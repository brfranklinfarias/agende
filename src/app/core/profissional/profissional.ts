import {Agenda} from "../agenda/agenda";
import {Injectable} from "@angular/core";
import {ProfissionalService} from "./profissional.service";

@Injectable({
  providedIn: 'root'
})

export class Profissional {
  id: number;
  nome: string;
  agenda: Agenda[];

  constructor(obj?: Object) {
    let object: any = Object.assign(obj);
    for (let prop in object) {
      this[prop] = object[prop];
    }
  }

  getAgenda(profissional: Profissional): Agenda[] {
    let agendas: Agenda[] = [];
    profissional.agenda.map(agenda => {
      agendas.push(agenda);
    });
    return agendas;
  }
}

