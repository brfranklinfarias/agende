export class User {
  name: string;
  email: string;
  cellphone: string;
  cpf: string;
  born: string;
  sexo: string;
  login: string;
  state: string;
  city: string;
  password: string;
  confirmePassword: string;

  constructor(obj?: Object) {
    let object: any = Object.assign(obj);
    for (let prop in object) {
      this[prop] = object[prop];
    }
  }
}
