import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable, Subject} from "rxjs";
import {User} from "./user";
import {environment} from "../../../environments/environment";
import {catchError} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  api = environment.api;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'Basic my-auth-token'
    })
  };

  constructor(private httpClient: HttpClient) {
  }

  list(user: User): Observable<User> {
    return this.httpClient.get<User>(`${this.api}/usuarios?login=${user.login}&password=${user.password}`, {responseType: "json"});
  }

  addUser(user: User): Observable<User> {
    return this.httpClient.post<User>(`${this.api}/usuarios`, user, this.httpOptions)
  }
}
