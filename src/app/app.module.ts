import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {LoginComponent} from './login/login.component';
import {ConsultaComponent} from './consulta/consulta.component';
import {HeaderComponent} from './header/header.component';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {MainComponent} from './main/main.component';
import {CustomTableComponent} from './custom-table/custom-table.component';
import {FormBuilder, FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AuthenticationService} from './core/authentication/authentication.service';
import {Authguard} from './core/authentication/authguard';
import {SolicitacaoComponent} from './solicitacao/solicitacao.component';
import {RegisterComponent} from './login/register/register.component';
import {AgendaListComponent} from './agenda/agenda-list.component';
import {HttpClientModule} from '@angular/common/http';
import { AlertComponent } from './alert/alert.component';
import {AlertModule} from './alert/alert.module';
import {AgendaService} from './core/agenda/agenda.service';
import { MenuComponent } from './menu/menu.component';
import {MenuModule} from './menu/menu.module';
import {NgxMaskModule} from 'ngx-mask';
import { SpinnerComponent } from './spinner/spinner.component';
import { HorariosComponent } from './horarios/horarios.component';
import { CollapseComponent } from './collapse/collapse.component';
import { TipoConsultaComponent } from './tipo-consulta/tipo-consulta.component';
import { ClienteComponent } from './cliente/cliente.component';
import { ModalComponent } from './modal/modal.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ConsultaComponent,
    HeaderComponent,
    MainComponent,
    CustomTableComponent,
    SolicitacaoComponent,
    RegisterComponent,
    AgendaListComponent,
    AlertComponent,
    MenuComponent,
    SpinnerComponent,
    HorariosComponent,
    CollapseComponent,
    TipoConsultaComponent,
    ClienteComponent,
    ModalComponent
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        FontAwesomeModule,
        FormsModule,
        HttpClientModule,
        ReactiveFormsModule,
        AlertModule,
        MenuModule,
        NgxMaskModule.forRoot()
    ],
  providers: [AuthenticationService, AgendaService, Authguard, FormBuilder],
  bootstrap: [AppComponent]
})
export class AppModule {}
