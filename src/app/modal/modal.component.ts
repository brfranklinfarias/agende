import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {faTimesCircle} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {

  @Output() close = new EventEmitter();
  @Input() isVisible = false;
  faTimesCicle = faTimesCircle;

  constructor() {
  }

  ngOnInit(): void {
  }

  closeModal() {
    this.close.emit(this.isVisible = false);
  }
}
