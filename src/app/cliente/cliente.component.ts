import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-cliente',
  templateUrl: './cliente.component.html',
  styleUrls: ['./cliente.component.scss']
})
export class ClienteComponent implements OnInit {

  tableHeader =
    [
      {id: 1, description: 'Nome'},
      {id: 2, description: 'E-mail'}
    ];
  clientes = [
    {
      id: 1,
      nome: 'Franklin Teixeira de Farias',
      email: 'franklinfarias1391@gmail.com'
    },
    {
      id: 2,
      nome: 'Samara Rizia Silva de Lima Farias',
      email: 'samara.wzdserraria@gmail.com'
    },
    {
      id: 3,
      nome: 'Zezinho da batata doce',
      email: 'zezinho@gmail.com'
    },
    {
      id: 4,
      nome: 'Zé do galinheiro',
      email: 'galinheiro@gmail.com'
    }
  ];

  constructor() {
  }

  ngOnInit(): void {
  }

}
