import {Component, OnInit, Renderer2, ViewChild} from '@angular/core';
import {
  faBars,
  faCalendarWeek,
  faIdCard,
  faSignOutAlt,
  faUserCircle,
  faSmile,
  faExclamation
} from "@fortawesome/free-solid-svg-icons";
import {Estado} from "../core/estado/estado";
import {Cidade} from "../core/estado/cidade/cidade";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {AlertType} from "../core/utility/alert/typeAlert";
import {Router} from "@angular/router";
import {AlertService} from "../core/utility/alert/alert.service";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @ViewChild('header', {static: false}) header;
  showMenuList: boolean;
  faUserCircle = faUserCircle;
  faSignOutAlt = faSignOutAlt;
  faBars = faBars;
  faIdCard = faIdCard;
  faSmile = faSmile;
  faExclamation = faExclamation;
  faCalendar = faCalendarWeek;
  isVisible = false;
  estado: Estado[] = [];
  cidades: Cidade[] = [];
  user = [];
  form = new FormGroup({
    name: new FormControl('', Validators.required),
    email: new FormControl('', Validators.required),
    cellphone: new FormControl('', Validators.required),
    cpf: new FormControl('', Validators.required),
    born: new FormControl('', Validators.required),
    sexo: new FormControl('', Validators.required),
    login: new FormControl('', Validators.required),
    state: new FormControl('', Validators.required),
    city: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required),
    confirmePassword: new FormControl('', Validators.required)
  });

  constructor(private render: Renderer2, private router: Router, private alertService: AlertService) {
  }

  ngOnInit(): void {
  }

  showMenu() {
    this.showMenuList = true;
    const menu = this.header.nativeElement.nextSibling.firstChild;
    this.render.removeClass(menu, 'hidden');
  }

  hiddenMenu() {
    this.showMenuList = false;
  }

  updateUser() {
    if (this.user) {
      this.isVisible = false;
      this.router.navigate(['/consulta']);
      setTimeout(() => {
        this.alertService.send({
          message: 'Atualizado com sucesso!',
          type: AlertType.Success,
          icon: faSmile
        });
      }, 200);
    }
  }

  toogle() {
    if (!this.isVisible) {
      this.isVisible = true;
    } else {
      this.isVisible = false;
    }
  }


}
