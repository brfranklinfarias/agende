import {Component, Input, OnChanges, Renderer2, SimpleChanges, ViewChild} from '@angular/core';
import {Alert} from '../core/utility/alert/alert';
import {AlertType} from '../core/utility/alert/typeAlert';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss']
})
export class AlertComponent implements OnChanges {
  @Input() alert = new Alert({});
  @ViewChild('containerAlert', {static: false}) containerAlert;

  constructor(private render: Renderer2) {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.containerAlert !== undefined) {
      this.render.removeClass(this.containerAlert.nativeElement, 'hidden');
      this.render.addClass(this.containerAlert.nativeElement, 'show');
    }
    switch (this.alert?.type) {
      case AlertType.Success:
        this.render.addClass(this.containerAlert.nativeElement, 'success');
        this.render.removeClass(this.containerAlert.nativeElement, 'error');
        this.render.removeClass(this.containerAlert.nativeElement, 'warning');
        setTimeout(() => {
          this.render.addClass(this.containerAlert.nativeElement, 'hidden');
        }, 3000);
        break;
      case AlertType.Error:
        console.log(this.containerAlert.nativeElement);
        this.render.addClass(this.containerAlert.nativeElement, 'error');
        this.render.removeClass(this.containerAlert.nativeElement, 'success');
        this.render.removeClass(this.containerAlert.nativeElement, 'warning');
        setTimeout(() => {
          this.render.addClass(this.containerAlert.nativeElement, 'hidden');
        }, 3000);
        break;
      case AlertType.Warning:
        this.render.addClass(this.containerAlert.nativeElement, 'warning');
        this.render.removeClass(this.containerAlert.nativeElement, 'error');
        this.render.removeClass(this.containerAlert.nativeElement, 'success');
        setTimeout(() => {
          this.render.addClass(this.containerAlert.nativeElement, 'hidden');
        }, 3000);
        break;
    }
  }
}
