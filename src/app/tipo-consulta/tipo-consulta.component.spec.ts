import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TipoConsultaComponent } from './tipo-consulta.component';

describe('TipoConsultaComponent', () => {
  let component: TipoConsultaComponent;
  let fixture: ComponentFixture<TipoConsultaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TipoConsultaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TipoConsultaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
