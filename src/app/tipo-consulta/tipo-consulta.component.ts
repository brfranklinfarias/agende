import { Component, OnInit } from '@angular/core';
import {AlertType} from '../core/utility/alert/typeAlert';
import {Router} from '@angular/router';
import {AlertService} from '../core/utility/alert/alert.service';
import {faSmile} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-tipo-consulta',
  templateUrl: './tipo-consulta.component.html',
  styleUrls: ['./tipo-consulta.component.scss']
})
export class TipoConsultaComponent implements OnInit {

  constructor(private router: Router, private alertService: AlertService) { }

  ngOnInit(): void {
  }

  save() {
    this.router.navigate(['/consulta']);
    setTimeout(() => {
      this.alertService.send({
        message: 'Cadastrado com sucesso!',
        type: AlertType.Success,
        icon: faSmile
      });
    }, 200);
  }

}
