import {Component, Input, OnInit} from '@angular/core';
import {faAddressBook} from '@fortawesome/free-solid-svg-icons/faAddressBook';
import {faClipboardList} from '@fortawesome/free-solid-svg-icons/faClipboardList';
import {faAddressCard, faClock, faUserFriends} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  @Input() isVisible;
  faAddressBook = faAddressBook;
  faAddresCard = faAddressCard;
  faClock = faClock;
  faUserFriends = faUserFriends;
  faClipboardList = faClipboardList;
  constructor() {
  }

  ngOnInit(): void {
  }

  hiddenMenu() {
    this.isVisible = false;
  }
}
