import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './consulta.component.html',
  styleUrls: ['./consulta.component.scss']
})
export class ConsultaComponent implements OnInit {

  @Input() userLogged: boolean;

  tableHeader =
    [
      {id: 1, description: 'Data da consulta'},
      {id: 2, description: 'Hora'},
      {id: 3, description: 'Procedimento'},
      {id: 4, description: 'Profissional'},
      {id: 5, description: 'Especialidade'},
      {id: 6, description: 'CRO'}
    ];

  consulta = [
    {
      id: 1,
      data: '27/03/2020',
      hora: '10:00',
      profissional:
        [
          {
            nome: 'Dr. José dos Santos',
            especialidade: 'Ortodontista',
            cro: '125489'
          }
        ],
      procedimento: 'Remoção de tartaro'
    },
    {
      id: 2,
      data: '27/04/2020',
      hora: '10:30',
      profissional:
        [
          {
            nome: 'Dr. José dos Santos',
            especialidade: 'Ortodontista',
            cro: '125489'
          }
        ],
      procedimento: 'Tratamento de canal dentário'
    },
    {
      id: 1,
      data: '27/03/2020',
      hora: '10:00',
      profissional:
        [
          {
            nome: 'Dr. José dos Santos',
            especialidade: 'Ortodontista',
            cro: '125489'
          }
        ],
      procedimento: 'Remoção de tartaro'
    },
    {
      id: 1,
      data: '27/03/2020',
      hora: '10:00',
      profissional:
        [
          {
            nome: 'Dr. José dos Santos',
            especialidade: 'Ortodontista',
            cro: '125489'
          }
        ],
      procedimento: 'Remoção de tartaro'
    },
    {
      id: 1,
      data: '27/03/2020',
      hora: '10:00',
      profissional:
        [
          {
            nome: 'Dr. José dos Santos',
            especialidade: 'Ortodontista',
            cro: '125489'
          }
        ],
      procedimento: 'Remoção de tartaro'
    },
    {
      id: 1,
      data: '27/03/2020',
      hora: '10:00',
      profissional:
        [
          {
            nome: 'Dr. José dos Santos',
            especialidade: 'Ortodontista',
            cro: '125489'
          }
        ],
      procedimento: 'Remoção de tartaro'
    },
    {
      id: 1,
      data: '27/03/2020',
      hora: '10:00',
      profissional:
        [
          {
            nome: 'Dr. José dos Santos',
            especialidade: 'Ortodontista',
            cro: '125489'
          }
        ],
      procedimento: 'Remoção de tartaro'
    },
    {
      id: 1,
      data: '27/03/2020',
      hora: '10:00',
      profissional:
        [
          {
            nome: 'Dr. José dos Santos',
            especialidade: 'Ortodontista',
            cro: '125489'
          }
        ],
      procedimento: 'Remoção de tartaro'
    },
    {
      id: 1,
      data: '27/03/2020',
      hora: '10:00',
      profissional:
        [
          {
            nome: 'Dr. José dos Santos',
            especialidade: 'Ortodontista',
            cro: '125489'
          }
        ],
      procedimento: 'Remoção de tartaro'
    },
    {
      id: 1,
      data: '27/03/2020',
      hora: '10:00',
      profissional:
        [
          {
            nome: 'Dr. José dos Santos',
            especialidade: 'Ortodontista',
            cro: '125489'
          }
        ],
      procedimento: 'Remoção de tartaro'
    },
    {
      id: 1,
      data: '27/03/2020',
      hora: '10:00',
      profissional:
        [
          {
            nome: 'Dr. José dos Santos',
            especialidade: 'Ortodontista',
            cro: '125489'
          }
        ],
      procedimento: 'Remoção de tartaro'
    },
    {
      id: 1,
      data: '27/03/2020',
      hora: '10:00',
      profissional:
        [
          {
            nome: 'Dr. José dos Santos',
            especialidade: 'Ortodontista',
            cro: '125489'
          }
        ],
      procedimento: 'Remoção de tartaro'
    }
  ];

  constructor() {
  }

  ngOnInit(): void {
  }

}
