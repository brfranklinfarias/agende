import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {User} from "../core/usuario/user";
import {AuthenticationService} from "../core/authentication/authentication.service";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {faLock, faUser} from "@fortawesome/free-solid-svg-icons";
import {UserService} from "../core/usuario/user.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  user: User = new User({});
  faUserSolid = faUser;
  faLock = faLock;
  form = new FormGroup({
    login: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required),
  });

  constructor(
    private router: Router, private formBuilder: FormBuilder,
    private authenticationService: AuthenticationService,
    private userService: UserService) {
  }

  ngOnInit(): void {
  }

  submitted() {
    const user = new User(this.form.value);
  }
}
