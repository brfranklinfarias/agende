import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {faSmile} from '@fortawesome/free-solid-svg-icons';
import {UserService} from '../../core/usuario/user.service';
import {User} from '../../core/usuario/user';
import {Router} from '@angular/router';
import {AlertType} from '../../core/utility/alert/typeAlert';
import {AlertService} from '../../core/utility/alert/alert.service';
import {EstadoService} from '../../core/estado/estado.service';
import {Estado} from '../../core/estado/estado';
import {Cidade} from '../../core/estado/cidade/cidade';
import {debounceTime} from 'rxjs/operators';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  faSmile = faSmile;
  estado: Estado[] = [];
  cidades: Cidade[] = [];
  form = new FormGroup({
    name: new FormControl('', Validators.required),
    email: new FormControl('', Validators.required),
    cellphone: new FormControl('', Validators.required),
    cpf: new FormControl('', Validators.required),
    born: new FormControl('', Validators.required),
    sexo: new FormControl('', Validators.required),
    login: new FormControl('', Validators.required),
    state: new FormControl('', Validators.required),
    city: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required),
    confirmePassword: new FormControl('', Validators.required)
  });

  constructor(private userService: UserService, private alertService: AlertService, private fb: FormBuilder,
              private router: Router, private estadoService: EstadoService) {
  }

  get name() {
    return this.form.get('name');
  }

  get email() {
    return this.form.get('email');
  }

  get cellphone() {
    return this.form.get('cellphone');
  }

  get cpf() {
    return this.form.get('cpf');
  }

  get born() {
    return this.form.get('born');
  }

  get sexo() {
    return this.form.get('sexo');
  }

  get login() {
    return this.form.get('login');
  }

  get state() {
    return this.form.get('state');
  }

  get city() {
    return this.form.get('city');
  }

  get password() {
    return this.form.get('password');
  }

  get confirmePassword() {
    return this.form.get('confirmePassword');
  }

  ngOnInit(): void {
    this.estadoService.getEstados().subscribe(e => {
      this.estado.push(e);
      this.estado = this.estado.reduce((estados, estado) => estados.concat(estado), []);
    });
    this.form.valueChanges.pipe(debounceTime(2000)).subscribe(control => {
      this.estadoService.getCidadesByUF(control.state).subscribe(cidade => {
        this.cidades = Object.values(cidade);
      });
    });

  }

  save() {
    if (this.form.valid) {
      setTimeout(() => {
        this.alertService.send({
          message: 'Cadastrado com sucesso!',
          type: AlertType.Success,
          icon: this.faSmile
        });
      }, 200);
      this.router.navigate(['/login']);
    } else {
      setTimeout(() => {
        this.alertService.send({
          message: 'Preencha todos os campos!',
          type: AlertType.Warning,
          icon: this.faSmile
        });
      }, 200);
    }
  }
}
