import {AfterViewInit, Component, ElementRef, OnInit, PipeTransform, Renderer2, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {
  faCalendarWeek,
  faChevronCircleLeft,
  faChevronCircleRight,
  faClock,
  faCloudSun,
  faExclamation,
  faMoon,
  faSadTear,
  faSmile,
  faSun
} from '@fortawesome/free-solid-svg-icons';
import {DatePipe, Location} from '@angular/common';
import {Agenda} from '../core/agenda/agenda';
import {AlertService} from '../core/utility/alert/alert.service';
import {FormBuilder, FormControl, Validators} from '@angular/forms';
import {Profissional} from '../core/profissional/profissional';
import {ProfissionalService} from '../core/profissional/profissional.service';
import {AlertType} from '../core/utility/alert/typeAlert';
import {AgendaService} from '../core/agenda/agenda.service';
import {HorariosService} from '../core/horarios/horarios.service';

@Component({
  selector: 'app-calendar',
  templateUrl: './agenda-list.component.html',
  styleUrls: ['./agenda-list.component.scss']
})
export class AgendaListComponent extends DatePipe implements OnInit, PipeTransform, AfterViewInit {

  @ViewChild('hora', {static: false}) hora: ElementRef;
  @ViewChild('dia', {static: false}) dia: ElementRef;
  faSad = faSadTear;
  faClock = faClock;
  faMoon = faMoon;
  faCloudSun = faCloudSun;
  faSun = faSun;
  faSmile = faSmile;
  faCalendar = faCalendarWeek;
  faExclamation = faExclamation;
  faChevronRight = faChevronCircleRight;
  faChevronLeft = faChevronCircleLeft;
  today = new Date().getDate();
  month;
  year = new Date().getFullYear();
  turno;
  dataAgenda;
  months = [
    {id: 0, descricao: 'Janeiro'},
    {id: 1, descricao: 'Fevereiro'},
    {id: 2, descricao: 'Março'},
    {id: 3, descricao: 'Abril'},
    {id: 4, descricao: 'Maio'},
    {id: 5, descricao: 'Junho'},
    {id: 6, descricao: 'Julho'},
    {id: 7, descricao: 'Agosto'},
    {id: 8, descricao: 'Setembro'},
    {id: 9, descricao: 'Outubro'},
    {id: 10, descricao: 'Novembro'},
    {id: 11, descricao: 'Dezembro'}
  ];
  week = [
    {id: 0, descricao: 'Dom'},
    {id: 1, descricao: 'Seg'},
    {id: 2, descricao: 'Ter'},
    {id: 3, descricao: 'Qua'},
    {id: 4, descricao: 'Quin'},
    {id: 5, descricao: 'Sex'},
    {id: 6, descricao: 'Sab'}
  ];
  days = [];
  dias = [];
  monthNumber;
  profissionalId;
  isAgenda;
  dataHora;
  agenda = new Agenda({});
  profissional = new Profissional({});
  form = this.fb.group({
    data: new FormControl('', Validators.required),
    horario: new FormControl('', Validators.required),
    tipoConsulta: new FormControl('', Validators.required)
  });
  isSpinner;
  hours = [];
  horariosAtendimento = [];
  diasAtendimento = [];
  numberMonth;
  day;
  agendamento;
  data = new Date();
  anoAtual = this.data.getFullYear();
  motherToday = new Date().getMonth();
  ano = new Date().getFullYear();

  constructor(private route: ActivatedRoute, private router: Router, private render: Renderer2, private fb: FormBuilder, private location: Location,
              private profissionalService: ProfissionalService, private alertService: AlertService, private agendaService: AgendaService, private horariosService: HorariosService) {
    super('');
  }

  ngOnInit(): void {
    this.turno = 'Manhã';
    this.profissionalId = this.route.snapshot.params.id;
    this.numberMonth = new Date().getMonth();
    this.horariosService.list().subscribe(horarios => {
      this.hours = horarios;
    });
    setTimeout(() => {
      this.totalDaysMonth(this.numberMonth, this.anoAtual);
      this.setDaysMonth(this.numberMonth, this.anoAtual);
    }, 200);
  }

  ngAfterViewInit(): void {
  }

  totalDaysMonth(mes?: number, ano?: number) {
    let totalDaysMonth;
    const getDaysInMonth = date => new Date(this.anoAtual, date.getMonth() + 1, 0).getDate();
    totalDaysMonth = getDaysInMonth(new Date(this.anoAtual, mes));
    let i = 0;
    do {
      this.days.push(i + 1);
      i++;
    } while (i < totalDaysMonth);
  }

  setDaysMonth(mes?: number, ano?: number) {
    this.days = [];
    this.dias = [];
    this.diasAtendimento = [];
    this.numberMonth = mes;
    this.totalDaysMonth(mes);
    for (let dt = 1; dt <= this.days.length; dt++) {
      const dia = new Date(this.ano, this.numberMonth, dt).getDay();
      const current = new Date(ano, this.numberMonth, dt).getDate();
      switch (dia) {
        case 0:
          this.dias.push({dia: current, descricao: 'domingo'});
          break;
        case 1:
          this.dias.push({dia: current, descricao: 'segunda'});
          break;
        case 2:
          this.dias.push({dia: current, descricao: 'terça'});
          break;
        case 3:
          this.dias.push({dia: current, descricao: 'quarta'});
          break;
        case 4:
          this.dias.push({dia: current, descricao: 'quinta'});
          break;
        case 5:
          this.dias.push({dia: current, descricao: 'sexta'});
          break;
        case 6:
          this.dias.push({dia: current, descricao: 'sábado'});
          break;
      }
    }

    this.isSpinner = true;
    setTimeout(() => {
      this.profissionalService.get(this.profissionalId).subscribe(profissional => {
        const dias = profissional.agenda.map(d => d.dia);
        dias.forEach(dia => {
          const diaAtendimento = this.dias.filter(d => d.descricao === dia);
          diaAtendimento.forEach(d => {
            this.diasAtendimento.push(d);
            this.diasAtendimento.sort((a, b) => {
              if (a.dia > b.dia) {
                return 1;
              }
              if (a.dia < b.dia) {
                return -1;
              }
            });
          });
        });
        this.isSpinner = false;
      });
    }, 2000);

    this.months.filter(m => {
      if (m.id === mes) {
        this.month = m.descricao;
      }
    });
  }

  getNextMonth(mes) {
    this.numberMonth = mes + 1;
    this.horariosAtendimento = [];
    if (this.numberMonth === 12) {
      this.numberMonth = 0;
      this.ano = this.ano + 1;
    }
    this.totalDaysMonth(this.numberMonth, this.anoAtual);
    this.setDaysMonth(this.numberMonth, this.anoAtual);
  }

  getPreviousMonth(mes) {
    this.numberMonth = mes - 1;
    this.horariosAtendimento = [];
    if (this.numberMonth === (-1)) {
      this.numberMonth = 11;
      this.ano = this.ano - 1;
    }
    this.totalDaysMonth(this.numberMonth, this.anoAtual);
    this.setDaysMonth(this.numberMonth, this.anoAtual);
  }

  setDay(day: number) {
    this.dataAgenda = day;
  }

  setData(data) {
    this.dataHora = {data, hora: ''};
  }

  setHora(hora) {
    this.dataHora.hora = hora;
  }

  getListHours(dia?: number) {
    let lisHours;
    this.dataAgenda = dia;
    const getDiaAtendimento = this.diasAtendimento.filter(agenda => this.dataAgenda === agenda.dia);
    this.profissionalService.get(this.profissionalId).subscribe(profissional => {
      getDiaAtendimento.forEach(d => {
        const diaAgenda = profissional.agenda.filter(agenda => agenda.dia === d.descricao);
        const horarioAtendimento = diaAgenda.reduce(day => day);
        lisHours = this.hours.filter(hour => hour.hora >= horarioAtendimento.horarioInicio && hour.hora <= horarioAtendimento.horarioFim);
        this.horariosAtendimento = lisHours;
      });
    });
  }

  getTipoConsulta() {
    console.log(this.form.controls);
  }

  getAgendamento(h) {
    const agendamento = {id: 1, dia: this.dataAgenda, horario: h};
    this.render.addClass(this.hora.nativeElement, 'clock-active');
    this.agendamento = agendamento;
  }

  save() {
    if (this.agendamento) {
      this.router.navigate(['/consulta']);
      setTimeout(() => {
        this.alertService.send({
          message: 'Agendado com sucesso!',
          type: AlertType.Success,
          icon: faSmile
        });
      }, 200);
    } else {
      setTimeout(() => {
        this.alertService.send({
          message: 'Por favor selecione um dia e uma hora!',
          type: AlertType.Warning,
          icon: this.faExclamation
        });
      }, 200);
    }
  }
}
