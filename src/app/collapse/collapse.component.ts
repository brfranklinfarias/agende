import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {faCalendar, faHistory, faSortDown, faSortUp} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-collapse',
  templateUrl: './collapse.component.html',
  styleUrls: ['./collapse.component.scss']
})
export class CollapseComponent implements OnInit {

  @Output() open = new EventEmitter();
  @Output() close = new EventEmitter();
  @Input() title;
  visible;

  faSortDown = faSortDown;
  faHistory = faHistory;
  faSortUp = faSortUp;

  constructor() {
  }

  ngOnInit(): void {
  }

  toogle() {
    this.visible = !this.visible;
    if (this.visible) {
      this.open.emit(this.visible);
    } else {
      this.close.emit(!this.visible);
    }
  }
}
