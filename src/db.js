const low = require('lowdb');
const FileSync = require('lowdb/adapters/FileSync');

const adapter = new FileSync('db.json');
const db = low(adapter);

// Set some defaults (required if your JSON file is empty)
db.defaults(
  {
    profissional: [],
    horarios: [],
    count: 0
  })
  .write();

// Add a post
db.get()
  .push()
  .write();

// Set a user using Lodash shorthand syntax
db.set()
  .write();

// Increment count
db.update('count', n => n + 1)
  .write();
